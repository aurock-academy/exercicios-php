Aurock Consultoria e Desenvolvimento
====================================

Exercícios de PHP
-----------------

Olá, seja bem vindo.

Para resolver os exercicíos, clone o repositório, utilizando sua própria conta do GitLab, e quando terminar, faça um "merge-request", sinalizando o termino dos exercicios para que possam ser analisados.

Algumas considerações:

- Cada exercício deve ser feito dentro do próprio arquivo já denominado;
- Cada exercício finalizado deve ser commitado separadamente. Ou seja, você vai commitar um exercício por vez;
- O código de apoio para o banco de dados utiliza SQLite, é possivel utilizar qualquer banco de dados SQL apenas modificando a linha numero `4` dos arquivos. Ex.: mudar de `sqlite:dbs/contatos.sq3` para `mysql:host=localhost;dbname=meubanco` para utilizar MySQL;
- Caso não consiga resolver os exercícios, submeta a solução parcial;
- Atente-se as recomendações no topo dos arquivos;

Exercicios:

- 1) Iteração: Exibir (utilizando echo) a matriz abaixo utilizando os três métodos: for, foreach, while e do-while;
- 2) Tabelas: Exibir a lista abaixo como uma tabela html, ordenando pelo numero de vitórias, e rounds ganhos (desempate);
- 3) Condicional: Exibir a lista abaixo como uma tabela html utilizando cores de acordo com o criterio;
- 4) Formulário: Desenvolver um formulario e exibir os valores preenchidos após o envio (escondendo o form);
- 5) Envio de email: Desenvolver um formulario e enviar por email os valores preenchidos;
- 6) Validação: Desenvolver um formulario e validar os dados enviados;
- 7) Leitura CSV: Fazer a leitura do arquivo CSV chamado "planilha.csv" (disponivel na raiz) e exibi-lo como tabela;
- 8) Upload de arquivos: Desenvolver um formulario para fazer upload de arquivos
- 9) Banco de dados: Desenvolver um formulario de contato (nome, email, telefone, mensagem), inserir os dados no banco e exibir todos os contatos inseridos em uma tabela;
- 10) Login: Criar um formulario de cadastro de usuários, fazer o login e redirecionamento;
- 11) Sessão: Adicionar a sessão no login e validar em todas as paginas de exercicio se o usuario está logado;
- 12) Menu dinamico: Exibir lista de todos os exercicios com link, dinamicamente;
- 13) Interpretação: leia o código escrito no exercicio e descreva o objetivo do código e seus passos;

Lembre-se: os exercicios são para a avaliação do seu conhecimento, portanto capriche!

Qualquer dúvida com relação aos exercícios, entre em contato conosco: recrutamento@aurock.com.br.

Até mais e Boa sorte!