<?php

/**
 * Exibir a lista abaixo como uma tabela html, ordenando pelo numero de vitórias, e rounds ganhos (desempate)
 * Colunas: Posição, Time, Jogos, Rounds Ganhos - Rounds Perdidos, Diferença Rounds (calcular a diferença)
 */

$challengers = [
    // time, vitorias, derrotas, rounds ganhos, rounds perdidos
    ['Team Liquid', '3', '0', '51', '30'],
    ['Ninjas in Pyjamas', '3', '0', '60', '43'],
    ['Astralis', '3', '1', '74', '48', '+26'],
    ['compLexity Gaming', '3', '1', '55', '54'],
    ['HellRaisers', '3', '1', '69', '70'],
    ['BIG', '2', '2', '60', '49'],
    ['North', '2', '2','71', '64'],
    ['TyLoo', '2', '2', '73', '71'],
    ['Team Spirit', '2', '2', '49', '49'],
    ['Vega Squadron', '2', '2', '67', '68'],
    ['OpTic Gaming', '2', '2', '71', '80'],
    ['Gambit Esports', '1', '3', '55', '61'],
    ['Rogue', '1', '3', '56', '67'],
    ['Renegades', '1', '3', '45', '63'],
    ['Space Soldiers', '0', '3', '36', '51'],
    ['Virtus.pro', '0', '3', '24', '48'],
];

?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <title>Challengers - Major</title>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>Posição</th>
                    <th>Time</th>
                    <th>Jogos</th>
                    <th>Rounds Ganhos - Rounds Perdidos</th>
                    <th>Diferença</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <a href="11-index.php"><br>Voltar</a>
        </table>
    </body>
</html>
