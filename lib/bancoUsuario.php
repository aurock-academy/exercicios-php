<?php

function connect() {
    return new PDO('sqlite:dbs/usuarios.sq3');
}

function select($conn, $sql) {
    $stm = $conn->query($sql);

    $data = [];
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
        $data[] = $row;
    }

    return $data;
}

function insert($conn, $sql) {
    return (bool) $conn->query($sql);
}

function createTable ($conn) {
    return $conn->exec(
        'CREATE TABLE IF NOT EXISTS USUARIO(
            id integer PRIMARY KEY AUTOINCREMENT,
            email varchar(50),
            senha varchar (40)
        );'
    );
}